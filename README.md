## CONTENTS OF THIS FILE

  - Introduction
  - Requirements
  - Installation
  - Configuration
  - Maintainers

## INTRODUCTION

Google Programmable Search Engine allows the creation of custom search
engines for a website. Integrating with the core Drupal search
functionality, this module provides for the creation of search pages
that use a JSON API to retrieve results from a Google custom search
engine.

Support for both the Google Custom Search JSON API and the Google Custom
Search Site Restricted JSON API is provided.

  - For a full description of the module, visit the project page
    <https://www.drupal.org/project/google_json_api>

  - To submit bug reports and feature suggestions, or to track changes
    <https://www.drupal.org/project/issues/search/google_json_api>

  - For information about Google Programmable Search Engines
    <https://developers.google.com/custom-search/docs/tutorial/introduction>

  - To create and manage Custom Search Engines
    <https://programmablesearchengine.google.com/cse/all>

  - For information about the Custom Search JSON API and retrieving an
    API key <https://developers.google.com/custom-search/v1/overview>

  - For information about the Custom Search Site Restricted JSON API
    <https://developers.google.com/custom-search/v1/site_restricted_api>

## REQUIREMENTS

Token module must be enabled to support customization of end-user
messages on the search results pages.

A Google Custom Search Engine must be set up. A Google API Key and a
Custom Search Engine id will are required for communication on the JSON
API.

## INSTALLATION

  - Install as you would normally install a contributed Drupal module.
    See <https://www.drupal.org/node/895232> for further information.

## CONFIGURATION

View/Modify Google Custom Search JSON API Endpoints: Home >
Administration > Configuration > Search & metadata > Google JSON API
Note that these endpoint values will likely not need modified.

Create and Manage Google JSON API Search Pages: Home > Administration
> Configuration > Search & metadata > Pages

Place and Configure the Search Form Block: Home > Administration >
Structure > Block

The module integrates with the core search pages functionality.

Core Search form block can be configured to use your custom search page.

## MAINTAINERS

Current maintainers: * Sam Lerner (SamLerner)
<https://www.drupal.org/u/samlerner>

  - Timothy Zura (tzura) <https://www.drupal.org/u/tzura>
