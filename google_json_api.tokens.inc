<?php

/**
 * @file
 * Google JSON API token support.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function google_json_api_token_info() {

  $type = [
    'name' => t('Google JSON API Search'),
    'description' => t('Google JSON API search results information.'),
    'needs-data' => 'array',
  ];

  $tokens['google_total_results_count'] = [
    'name' => t('Google Total Results Count'),
    'description' => t('The total number of Google Results available.'),
  ];

  $tokens['google_json_api_search_keywords'] = [
    'name' => t('Google JSON API Search Keywords'),
    'description' => t('The keywords used in a Google JSON API search.'),
  ];

  $tokens['google_json_api_result_start'] = [
    'name' => t('Google JSON API Result Starting Number'),
    'description' => t('The number of the first result being displayed on a page.'),
  ];

  $tokens['google_json_api_result_end'] = [
    'name' => t('Google JSON API Result Ending Number'),
    'description' => t('The number of the last result being displayed on a page.'),
  ];

  $tokens['google_json_api_sort_value'] = [
    'name' => t('Google JSON API Sort Value'),
    'description' => t('The current sort value in a Google JSON API search.'),
  ];

  $tokens['google_json_api_search_page'] = [
    'name' => t('Google JSON API Search Page Name'),
    'description' => t('The current search page name of a Google JSON API search.'),
  ];

  return [
    'types' => ['google_json_api' => $type],
    'tokens' => [
      'google_json_api' => $tokens,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function google_json_api_tokens($type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $replacements = [];

  if ($type == 'google_json_api' && !empty($data['google_json_api'])) {

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'google_total_results_count':
          $replacements[$original] = $data['google_json_api']['google_total_results'];
          break;

        case 'google_json_api_search_keywords':
          $replacements[$original] = $data['google_json_api']['google_json_api_search_keywords'];
          break;

        case 'google_json_api_result_start':
          $replacements[$original] = $data['google_json_api']['google_json_api_result_start'];
          break;

        case 'google_json_api_result_end':
          $replacements[$original] = $data['google_json_api']['google_json_api_result_end'];
          break;

        case 'google_json_api_sort_value':
          $replacements[$original] = $data['google_json_api']['google_json_api_sort_value'];
          break;

        case 'google_json_api_search_page':
          $replacements[$original] = $data['google_json_api']['google_json_api_search_page'];
          break;

      }
    }
  }

  return $replacements;
}
