// Add listener to search page select element of SERP search form.
// Selection of page will modify the action attribute of the form.
if(document.getElementById('google-json-api-search-box-form').searchpage) {
    document.getElementById('google-json-api-search-box-form').searchpage.onchange = function() {
        var newaction = this.value;
        document.getElementById('google-json-api-search-box-form').action = newaction;
    };
}
