<?php

namespace Drupal\google_json_api\TwigExtension;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

/**
 * Class HighlightSearchTerm create Twig function to highlight search term.
 */
class HighlightSearchTerm extends AbstractExtension {

  /**
   * Generates a list of all Twig functions that this extension defines.
   *
   * @return array
   *   A key/value array that defines custom Twig functions. The key denotes the
   *   function name used in the tag, e.g.:
   *   @code
   *   {{ testfunc() }}
   *   @endcode
   *
   *   The value is a standard PHP callback that defines what the function does.
   */
  public function getFunctions(): array {
    return [new TwigFunction('highlight_search', [$this, 'highlightSearch'])];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName(): string {
    return 'google_json_api.highlight_search_term';
  }

  /**
   * Function to bold search terms in a snippet of text.
   *
   * @param string|null $string
   *   HTML of search result as string.
   * @param string|null $term
   *   The search term to highlight.
   */
  public static function highlightSearch(string|null $string, string $term = NULL): void {
    if (empty($string)) {
      echo '';
    }
    elseif (!empty($term)) {
      $bold_term = '<strong>' . $term . '</strong>';
      $new_string = '<span>' . str_ireplace($term, $bold_term, $string) . '</span>';
      echo $new_string;
    }
  }

}
