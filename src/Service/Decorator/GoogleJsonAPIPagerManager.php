<?php

namespace Drupal\google_json_api\Service\Decorator;

use Drupal\Core\Pager\PagerManager;

/**
 * A Decorator for PagerManager supporting Google Custom Search.
 *
 * Google Custom Search only returns 100 results, even if
 * there are thousands of results found.  This class modifies
 * the search pager so that it shows the total number of
 * Google results, while limiting the pager to 10 pages.
 */
class GoogleJsonAPIPagerManager extends PagerManager {

  /**
   * {@inheritdoc}
   */
  public function createGooglePager($total, $limit, $element = 0) {
    $currentPage = $this->pagerParams->findPage($element);

    // $total pages is the number of Google search results.
    $pager = new GoogleJsonAPIPager($total, $limit, $currentPage);
    $this->setPager($pager, $element);
    return $pager;
  }

}
