<?php

namespace Drupal\google_json_api\Service\Decorator;

use Drupal\Core\Pager\Pager;

/**
 * A value object that represents a pager.
 */
class GoogleJsonAPIPager extends Pager {

  /**
   * The total number of Google Results.
   *
   * @var int
   */
  protected $totalGoogleItems;

  /**
   * Pager constructor.
   *
   * @param int $totalGoogleItems
   *   The total number of items returned by Google.
   * @param int $limit
   *   The maximum number of items per page.
   * @param int $currentPage
   *   The current page.
   */
  public function __construct($totalGoogleItems, $limit, $currentPage = 0) {
    $this->totalItems = min($totalGoogleItems, 100);
    $this->totalGoogleItems = $totalGoogleItems;
    $this->limit = $limit;
    $this->setTotalPages($this->totalItems, $limit);
    $this->setCurrentPage($currentPage);
  }

  /**
   * Gets the total number of Google items.
   *
   * @return int
   *   The total number of Google items.
   */
  public function getTotalGoogleItems() {
    return $this->totalGoogleItems;
  }

}
