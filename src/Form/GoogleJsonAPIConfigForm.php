<?php

namespace Drupal\google_json_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides administration/configuration form for module settings.
 *
 * @package Drupal\google_json_api\Form
 */
class GoogleJsonAPIConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_json_api_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['search_json_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Programmable Search JSON API Endpoint:'),
      '#default_value' => $this->config('google_json_api.settings')->get('search_json_endpoint'),
      '#description' => $this->t('Refer to <a href=":jsonapidocs">Google Programmable Search
        JSON API Documentation</a>.',
        [':jsonapidocs' => $this->config('google_json_api.settings')->get('google_json_api.google_json_api_documentation')]),
    ];

    $form['search_site_restricted_json_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Programmable Search Site Restricted JSON API Endpoint:'),
      '#default_value' => $this->config('google_json_api.settings')->get('search_site_restricted_json_endpoint'),
      '#description' => $this->t('Refer to <a href=":jsonapidocs">Google Programmable Search Site Restricted
        JSON API Documentation</a>.',
        [':jsonapidocs' => $this->config('google_json_api.settings')->get('google_json_api.google_json_api_restricted_documentation')]),
    ];

    $form['google_programmable_search_control_panel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Programmable Search Engine Control Panel:'),
      '#default_value' => $this->config('google_json_api.settings')->get('google_programmable_search_control_panel'),
      '#description' => $this->t('URL to Google Programmable Search Control Panel.'),
    ];

    $form['google_programmable_search_documentation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Programmable Search Overview:'),
      '#default_value' => $this->config('google_json_api.settings')->get('google_programmable_search_documentation'),
      '#description' => $this->t('Link to the Google Programmable Search JSON API overview.'),
    ];

    $form['google_json_api_documentation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Programmable Search JSON API Documentation:'),
      '#default_value' => $this->config('google_json_api.settings')->get('google_json_api_documentation'),
      '#description' => $this->t('Link to the Google Programmable Search JSON API Documentation'),
    ];

    $form['google_json_api_restricted_documentation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Programmable Search Site Restricted JSON API Documentation:'),
      '#default_value' => $this->config('google_json_api.settings')->get('google_json_api_restricted_documentation'),
      '#description' => $this->t('Link to the Google Programmable Search Site Restricted JSON API Documentation'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('google_json_api.settings')->set('search_json_endpoint',
      $form_state->getValue('search_json_endpoint'))->save();
    $this->config('google_json_api.settings')->set('search_site_restricted_json_endpoint',
      $form_state->getValue('search_site_restricted_json_endpoint'))->save();
    $this->config('google_json_api.settings')->set('google_programmable_search_control_panel',
      $form_state->getValue('google_programmable_search_control_panel'))->save();
    $this->config('google_json_api.settings')->set('google_programmable_search_documentation',
      $form_state->getValue('google_programmable_search_documentation'))->save();
    $this->config('google_json_api.settings')->set('google_json_api_documentation',
      $form_state->getValue('google_json_api_documentation'))->save();
    $this->config('google_json_api.settings')->set('google_json_api_restricted_documentation',
      $form_state->getValue('google_json_api_restricted_documentation'))->save();

    parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'google_json_api.settings',
    ];
  }

}
