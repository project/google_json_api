<?php

/**
 * @file
 * Themeable functions for Google Custom Search JSON API results.
 */

/**
 * The search results page can be themed/customized.
 */
function template_preprocess_google_json_api_results(&$variables) {
}

/**
 * Search result item for Site Restricted Search API queries.
 */
function template_preprocess_google_json_api_result(&$variables) {

}

/**
 * Search results message for Site Restricted Search API queries.
 */
function template_preprocess_google_json_api_results_message(&$variables) {

}

/**
 * Search no results message for Site Restricted Search API queries.
 */
function template_preprocess_google_json_api_no_results_message(&$variables) {

}

/**
 * Search no keywords message for Site Restricted Search API queries.
 */
function template_preprocess_google_json_api_no_keywords_message(&$variables) {

}

/**
 * Search promoted result for Site Restricted Search API queries.
 */
function template_preprocess_google_json_api_promoted_result(&$variables) {

}

/**
 * Search spelling correction for Site Restricted Search API queries.
 */
function template_preprocess_google_json_api_spelling_correction(&$variables) {

}

/**
 * Search results limitation message for Site Restricted Search API queries.
 */
function template_preprocess_google_json_api_results_limitation_message(&$variables) {

}

/**
 * Search results page search form for Site Restricted Search API queries.
 */
function template_preprocess_google_json_api_search_page_form(&$variables) {
  $variables['form']['keys']['#attributes']['class'][] = 'gjas-element-keys';
  $variables['form']['keys']['#theme_wrappers'] = [];
  $variables['form']['sort']['#attributes']['class'][] = 'gjas-element-sort';
  $variables['form']['sort']['#theme_wrappers'] = [];
  $variables['form']['searchpage']['#attributes']['class'][] = 'gjas-element-page';
  $variables['form']['searchpage']['#theme_wrappers'] = [];
  $variables['form']['submit']['#attributes']['class'][] = 'gjas-element-submit';
}
