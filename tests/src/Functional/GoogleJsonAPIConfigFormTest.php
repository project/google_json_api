<?php

namespace Drupal\Tests\google_json_api\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\Url;

/**
 * Test the module settings page.
 *
 * @group google_json_api
 */
class GoogleJsonAPIConfigFormTest extends BrowserTestBase {

  /**
   * The default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'google_json_api',
  ];

  /**
   * Tests access to the Setting Form.
   */
  public function testAccessToSettingsForm() {

    $admin_user = $this->drupalCreateUser([
      'administer google json api',
    ]);

    $web_assert = $this->assertSession();

    // Login as our account.
    $this->drupalLogin($admin_user);

    // Get the settings form path from the route.
    $settings_form_path = Url::fromRoute('google_json_api.settings');

    // Navigate to the settings form.
    $this->drupalGet($settings_form_path);

    // Assure we loaded settings form with proper permissions.
    $web_assert->statusCodeEquals(200);

    $web_assert->fieldValueEquals('search_json_endpoint', NULL);

    // Logout of the account.
    $this->drupalLogout($admin_user);

    // Reload the page.
    $this->drupalGet($settings_form_path);

    // Assure no access as not being logged in.
    $web_assert->statusCodeEquals(403);

  }

  /**
   * Tests the Setting Form Submittal.
   */
  public function testSubmitOfForm() {

    $admin_user = $this->drupalCreateUser([
      'administer google json api',
    ]);

    $web_assert = $this->assertSession();

    // Login as our account.
    $this->drupalLogin($admin_user);

    // Get the settings form path from the route.
    $settings_form_path = Url::fromRoute('google_json_api.settings');

    // Navigate to the settings form.
    $this->drupalGet($settings_form_path);

    // Assure we loaded settings with proper permissions.
    $web_assert->statusCodeEquals(200);

    $this->submitForm([
      'search_json_endpoint' => 'https://www.googleapis.com/customsearch/v1',
      'search_site_restricted_json_endpoint' => 'https://www.googleapis.com/customsearch/v1/siterestrict',
      'google_custom_search_documentation' => 'https://developers.google.com/custom-search/v1/overview',
      'google_json_api_documentation' => 'https://developers.google.com/custom-search/v1/using_rest',
      'google_json_api_restricted_documentation' => 'https://developers.google.com/custom-search/v1/site_restricted_api',
    ], 'Save configuration');

    // Navigate to the settings form.
    $this->drupalGet($settings_form_path);

    $web_assert->fieldValueEquals('search_json_endpoint', 'https://www.googleapis.com/customsearch/v1');
    $web_assert->fieldValueEquals('search_site_restricted_json_endpoint', 'https://www.googleapis.com/customsearch/v1/siterestrict');
    $web_assert->fieldValueEquals('google_custom_search_documentation', 'https://developers.google.com/custom-search/v1/overview');
    $web_assert->fieldValueEquals('google_json_api_documentation', 'https://developers.google.com/custom-search/v1/using_rest');
    $web_assert->fieldValueEquals('google_json_api_restricted_documentation', 'https://developers.google.com/custom-search/v1/site_restricted_api');

    // Logout of the account.
    $this->drupalLogout($admin_user);

  }

}
