<?php

namespace Drupal\Tests\google_json_api\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\Url;

/**
 * Test search pages form.
 *
 * @group google_json_api
 */
class GoogleJsonAPISearchPageMgmtTest extends BrowserTestBase {

  /**
   * The default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'google_json_api',
  ];

  /**
   * Make sure search page form has google_json_api option.
   */
  public function testOptionOnSearchPagesForm() {

    $admin_user = $this->drupalCreateUser([
      'administer search',
    ]);

    $web_assert = $this->assertSession();

    // Login as our account.
    $this->drupalLogin($admin_user);

    // Get the search pages form path from the route.
    $searchpage_form_path = Url::fromRoute('entity.search_page.collection');

    // Navigate to the search pages form.
    $this->drupalGet($searchpage_form_path);

    // Assure we loaded search pages page with proper permissions.
    $web_assert->statusCodeEquals(200);

    $searchtypeoptions = $this->getOptions('search_type');

    $this->assertTrue(in_array('google_json_api_search', array_flip($searchtypeoptions)), 'Google Json API search page option available.');

    // Logout of the account.
    $this->drupalLogout($admin_user);

  }

  /**
   * Create search page using search page add form.
   */
  public function testCreateSearchPage() {

    $admin_user = $this->drupalCreateUser([
      'administer search',
    ]);

    $web_assert = $this->assertSession();

    // Login as our account.
    $this->drupalLogin($admin_user);

    // Get the search pages form path from the route.
    $searchpage_form_path = Url::fromRoute('search.add_type',
      ['search_plugin_id' => 'google_json_api_search']
    );

    // Navigate to the search add page form.
    $this->drupalGet($searchpage_form_path);

    // Assure we loaded search page add page with proper permissions.
    $web_assert->statusCodeEquals(200);

    $this->submitForm([
      'label' => 'Sample GJA Search Page',
      'id' => 'sample_gja_search_page',
      'path' => 'samplegja',
      'cx' => 'fakecxkey',
      'apikey' => 'fakeapikey',
      'apiendpoint' => 'search_site_restricted_json_endpoint',
      'resultsperpage' => 20,
    ], 'Save');

    $this->container->get('router.builder')->rebuild();

    $searchpages = Url::fromRoute('entity.search_page.collection');

    // Navigate to the new search page.
    $this->drupalGet($searchpages);

    $web_assert->statusCodeEquals(200);
    $web_assert->pageTextContains("Sample GJA Search Page");

    // Logout of the account.
    $this->drupalLogout($admin_user);

  }

}
